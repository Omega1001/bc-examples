package bc.microservice.services;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.Future;

import javax.enterprise.context.ApplicationScoped;

import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import bc.microservice.entities.CreditCondition;

@ApplicationScoped
public class ResultCollector {
	
	private static final Logger LOG = LoggerFactory.getLogger(ResultCollector.class);
	private ConcurrentMap<UUID, CompletableFuture<CreditCondition>>	watches = new ConcurrentHashMap<>();
	
	
	public Future<CreditCondition> watch(UUID contextId){
		LOG.info("Waiting on CreditConditions for {}",contextId);
		return watches.computeIfAbsent(contextId, e -> new CompletableFuture<>());
	}
	
	@Incoming("creditCalculated")
	public void collect(CreditCondition condition) {
		CompletableFuture<CreditCondition> watch = watches.remove(condition.getContextId());
		if(watch != null) {
			LOG.info("CreditConditions for {} are now available",condition.getContextId());
			watch.complete(condition);
		}
	}
	
} 
