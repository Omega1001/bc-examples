package bc.microservice.services;

import java.util.Random;

import javax.enterprise.context.ApplicationScoped;

import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.reactive.messaging.Outgoing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import bc.microservice.entities.Geolocation;
import bc.microservice.entities.LocalCreditRating;

@ApplicationScoped
public class LocalCreditScoreIndex {

	private static final Logger LOG = LoggerFactory.getLogger(LocalCreditScoreIndex.class);
	private static final Random RANDOM = new Random(System.currentTimeMillis());

	@Incoming("geolocalised")
	@Outgoing("localCreditScore")
	public LocalCreditRating getLocalRating(Geolocation loc) {
		LOG.info("Loading Credit score of region for {}", loc.getContextId());
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
		}
		LOG.info("Finished Loading Credit score of region for {}", loc.getContextId());
		return new LocalCreditRating(loc.getContextId(), RANDOM.nextDouble());
	}

}
