package bc.microservice.services;

import java.util.Random;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.eclipse.microprofile.context.ManagedExecutor;
import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import bc.microservice.entities.CreditConditionRequest;
import bc.microservice.entities.SchufaRating;

@ApplicationScoped
public class SchufaConnector {

	private static final Logger LOG = LoggerFactory.getLogger(SchufaConnector.class);
	private static final Random RANDOM = new Random(System.currentTimeMillis());

	@Inject
	@Channel("schufa")
	private Emitter<SchufaRating> schufaRating;
	
	@Inject
	private ManagedExecutor service;
	
	@Incoming("creditConditionsRequested")
	public void schufaRequested(CreditConditionRequest request) {
		
		service.submit(()->getSchufa(request));
		
	}
		
	
	public void getSchufa(CreditConditionRequest request) {
		LOG.info("Reading infos from Schufa for {}", request.getContextId());
		try {
			Thread.sleep(7000);
		} catch (InterruptedException e) {
		}
		LOG.info("Finished Reading infos from Schufa for {}", request.getContextId());
		schufaRating.send(new SchufaRating(request.getContextId(), RANDOM.nextDouble()));
	}

}
