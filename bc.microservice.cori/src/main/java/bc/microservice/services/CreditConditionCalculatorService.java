package bc.microservice.services;

import javax.enterprise.context.ApplicationScoped;

import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.reactive.messaging.Outgoing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import bc.microservice.entities.CreditCondition;
import bc.microservice.entities.CreditConditionData;

@ApplicationScoped
public class CreditConditionCalculatorService {

	private static final Logger LOG = LoggerFactory.getLogger(CreditConditionCalculatorService.class);

	@Incoming("conditionsGathered")
	@Outgoing("creditCalculated")
	public CreditCondition calculateConditions(CreditConditionData data) {
		LOG.info("Calculating Conditions for {}", data.getContextId());
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
		}
		LOG.info("Finished Calculating Conditions for {}", data.getContextId());
		return new CreditCondition(data.getContextId(),
				data.getSchufaRating().getRating() * data.getLocalRating().getRaing());
	}

}
