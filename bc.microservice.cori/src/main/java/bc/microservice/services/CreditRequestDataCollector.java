package bc.microservice.services;

import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import bc.microservice.entities.CreditConditionData;
import bc.microservice.entities.LocalCreditRating;
import bc.microservice.entities.SchufaRating;

@ApplicationScoped
public class CreditRequestDataCollector {

	private static final Logger LOG = LoggerFactory.getLogger(CreditRequestDataCollector.class);
	private ConcurrentMap<UUID, CreditConditionData> collector = new ConcurrentHashMap<>();

	@Inject
	@Channel("conditionsGathered")
	private Emitter<CreditConditionData> calculateCreditConditions;

	@Incoming("schufa")
	public void putSchufa(SchufaRating rating) {
		CreditConditionData entry = new CreditConditionData(rating.getContextId(), null, rating);
		entry = collector.merge(rating.getContextId(), entry , CreditConditionData::merge);
		LOG.info("Collected Schufa result for {}",rating.getContextId());
		if(entry.isComplete()) {
			collector.remove(entry.getContextId());
			forward(entry);
		}
	}

	@Incoming("localCreditScore")
	public void putLocalCreditRating(LocalCreditRating rating) {
		CreditConditionData entry = new CreditConditionData(rating.getContextId(), rating, null);
		entry = collector.merge(rating.getContextId(), entry, CreditConditionData::merge);
		LOG.info("Collected LocalCreditRating result for {}",rating.getContextId());
		if (entry.isComplete()) {
			collector.remove(entry.getContextId());
			forward(entry);
		}
	}

	public void forward(CreditConditionData data) {
		LOG.info("Credit Condition Data for {} is now complet, forwarding it",data.getContextId());
		calculateCreditConditions.send(data);
	}

}
