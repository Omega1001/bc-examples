package bc.microservice.services;

import java.util.Random;

import javax.enterprise.context.ApplicationScoped;

import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.reactive.messaging.Outgoing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import bc.microservice.entities.CreditConditionRequest;
import bc.microservice.entities.Geolocation;

@ApplicationScoped
public class GeolocationService {

	private static final Random RANDOM = new Random(System.currentTimeMillis());

	private static final Logger LOG = LoggerFactory.getLogger(GeolocationService.class);

	@Incoming("creditConditionsRequested")
	@Outgoing("geolocalised")
	public Geolocation adressToGeo(CreditConditionRequest request) {
		LOG.info("Converting adress to geo for {}", request.getContextId());
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
		}
		LOG.info("Finished Converting adress to geo for {}", request.getContextId());
		return new Geolocation(request.getContextId(), RANDOM.nextDouble(), RANDOM.nextDouble());
	}

}
