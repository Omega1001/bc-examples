package bc.microservice.services;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import bc.microservice.entities.CreditCondition;
import bc.microservice.entities.CreditConditionRequest;
import io.smallrye.reactive.messaging.annotations.Broadcast;

@Path("ccCalc")
public class Endpoint {

	private static final Logger LOG = LoggerFactory.getLogger(Endpoint.class);
	
	@Channel("creditConditionsRequested")
	@Broadcast
	@Inject
	private Emitter<CreditConditionRequest> schufaEmitter;


	@Inject
	private ResultCollector resColl;

	@POST
	@Path("conditions")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public CreditCondition calculateCreditRating(CreditConditionRequest request)
			throws InterruptedException, ExecutionException {

		LOG.info("Begin work for Credit Condition Requets {}", request.getContextId());
		
		Future<CreditCondition> resultFuture = resColl.watch(request.getContextId());
		
		schufaEmitter.send(request);
//		localScoreEmitter.send(request);
		
		CreditCondition creditConditions = resultFuture.get();
		
		LOG.info("Finished Work for Credit Condition Request {} : {}", request.getContextId(), creditConditions);
		return creditConditions;

	}
}
