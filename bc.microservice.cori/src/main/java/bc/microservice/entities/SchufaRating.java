package bc.microservice.entities;

import java.util.UUID;

public class SchufaRating {
	private UUID contextId;
	private double rating;
	
	public SchufaRating() {
		// TODO Auto-generated constructor stub
	}

	public SchufaRating(UUID contextId, double rating) {
		super();
		this.contextId = contextId;
		this.rating = rating;
	}

	public UUID getContextId() {
		return contextId;
	}

	public void setContextId(UUID contextId) {
		this.contextId = contextId;
	}

	public double getRating() {
		return rating;
	}

	public void setRating(double rating) {
		this.rating = rating;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SchufaRating [");
		if (contextId != null)
			builder.append("contextId=").append(contextId).append(", ");
		builder.append("rating=").append(rating).append("]");
		return builder.toString();
	}

	
	
	
	
}
