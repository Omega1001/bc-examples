package bc.microservice.entities;

import java.util.UUID;

public class LocalCreditRating {

	private UUID contextId;
	private double raing;

public LocalCreditRating() {
	// TODO Auto-generated constructor stub
}

@Override
public String toString() {
	StringBuilder builder = new StringBuilder();
	builder.append("LocalCreditRating [");
	if (contextId != null)
		builder.append("contextId=").append(contextId).append(", ");
	builder.append("raing=").append(raing).append("]");
	return builder.toString();
}

public UUID getContextId() {
	return contextId;
}

public void setContextId(UUID contextId) {
	this.contextId = contextId;
}

public double getRaing() {
	return raing;
}

public void setRaing(double raing) {
	this.raing = raing;
}

public LocalCreditRating(UUID contextId, double raing) {
	super();
	this.contextId = contextId;
	this.raing = raing;
}

}
