# BC-Examples

Projects run on Quarkus

# How to build and run

## Build with maven:

`mvn clean package`

## Run on Quarkus using maven

`mvn quarkus:run`

or for development (with hotdeploy)

`mvn quarkus:dev`

You can also build and run at once:

`mvn clean package quarkus:run`

# Sending a request
Quarkus runs on **port 8080** by default
To send an example Request to the service, use the Endpoint `ccCalc/conditions`  
The Endpoint expects a **HTTP POST** with data formated as `application/json`  
Data look like:

```
	{"address" : "xxx"}
```

Please not, that this is an example.
Thus the adress is not actually used in any way, so the actual value is irrelevant.
## Example request with CURL

```
curl --header 'Content-Type: application/json' --request POST --data '{"address" : "xxx"}' localhost:8080/ccCalc/conditions
```

Note: curl call is written for usage under Linux Bash shell.  
**Windows Users** may need to adapt the escapeSequence, as '' (single quotes) are not supported on all (any?) Windows shells .
