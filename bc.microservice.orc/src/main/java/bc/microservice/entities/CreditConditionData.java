package bc.microservice.entities;

import java.util.UUID;

public class CreditConditionData {

	private UUID contextId;
	private LocalCreditRating localRating;
	private SchufaRating schufaRating;
	
	public CreditConditionData() {
	}

	public CreditConditionData(UUID contextId, LocalCreditRating localRating, SchufaRating schufaRating) {
		super();
		this.contextId = contextId;
		this.localRating = localRating;
		this.schufaRating = schufaRating;
	}
	
	public static CreditConditionData merge(CreditConditionData source, CreditConditionData target) {
		if(target.localRating == null) {
			target.localRating = source.localRating;
		}
		if(target.schufaRating == null) {
			target.schufaRating = source.schufaRating;
		}
		return target;
	}
	
	public boolean isComplete() {
		return localRating != null && schufaRating != null;
	}

	public UUID getContextId() {
		return contextId;
	}

	public void setContextId(UUID contextId) {
		this.contextId = contextId;
	}

	public LocalCreditRating getLocalRating() {
		return localRating;
	}

	public void setLocalRating(LocalCreditRating localRating) {
		this.localRating = localRating;
	}

	public SchufaRating getSchufaRating() {
		return schufaRating;
	}

	public void setSchufaRating(SchufaRating schufaRating) {
		this.schufaRating = schufaRating;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CreditConditionData [");
		if (contextId != null)
			builder.append("contextId=").append(contextId).append(", ");
		if (localRating != null)
			builder.append("localRating=").append(localRating).append(", ");
		if (schufaRating != null)
			builder.append("schufaRating=").append(schufaRating);
		builder.append("]");
		return builder.toString();
	}

	
	
	
	
}
