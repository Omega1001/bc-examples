package bc.microservice.entities;

import java.util.UUID;

public class CreditCondition {

	private UUID contextId;
	private double rate;

	public CreditCondition() {
		super();
	}

	public CreditCondition(UUID contextId, double rate) {
		super();
		this.contextId = contextId;
		this.rate = rate;
	}

	public UUID getContextId() {
		return contextId;
	}

	public void setContextId(UUID contextId) {
		this.contextId = contextId;
	}

	public double getRate() {
		return rate;
	}

	public void setRate(double rate) {
		this.rate = rate;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CreditCondition [");
		if (contextId != null)
			builder.append("contextId=").append(contextId).append(", ");
		builder.append("rate=").append(rate).append("]");
		return builder.toString();
	}

}
