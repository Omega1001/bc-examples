package bc.microservice.entities;

import java.util.UUID;

public class Geolocation {
	
	private UUID contextId;
	private double latitude;
	private double longitude;

	public Geolocation() {
		
	}

	public Geolocation(UUID contextId, double latitude, double longitude) {
		super();
		this.contextId = contextId;
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public UUID getContextId() {
		return contextId;
	}

	public void setContextId(UUID contextId) {
		this.contextId = contextId;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Geolocation [");
		if (contextId != null)
			builder.append("contextId=").append(contextId).append(", ");
		builder.append("latitude=").append(latitude).append(", longitude=").append(longitude).append("]");
		return builder.toString();
	}

	

}
