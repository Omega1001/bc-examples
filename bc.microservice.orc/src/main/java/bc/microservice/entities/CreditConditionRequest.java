package bc.microservice.entities;

import java.util.UUID;

public class CreditConditionRequest {

	private UUID contextId = UUID.randomUUID();
	private String address;
	
	public CreditConditionRequest() {
	}

	public CreditConditionRequest(UUID contextId, String address) {
		super();
		this.contextId = contextId;
		this.address = address;
	}

	public UUID getContextId() {
		return contextId;
	}

	public void setContextId(UUID contextId) {
		this.contextId = contextId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CreditConditionRequest [");
		if (contextId != null)
			builder.append("contextId=").append(contextId).append(", ");
		if (address != null)
			builder.append("address=").append(address);
		builder.append("]");
		return builder.toString();
	}

	
	
	
	
}
