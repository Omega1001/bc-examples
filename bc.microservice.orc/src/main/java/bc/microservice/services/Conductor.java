package bc.microservice.services;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.context.ManagedExecutor;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import bc.microservice.entities.CreditCondition;
import bc.microservice.entities.CreditConditionData;
import bc.microservice.entities.CreditConditionRequest;
import bc.microservice.entities.Geolocation;
import bc.microservice.entities.LocalCreditRating;
import bc.microservice.entities.SchufaRating;

@Path("/ccCalc")
public class Conductor {

	private static final Logger LOG = LoggerFactory.getLogger(Conductor.class);
	@Inject
	private ManagedExecutor executor;

	@Inject
	@RestClient
	private ConditionsCalculatorClient conditionCalculator;

	@Inject
	@RestClient
	private GeoLocalisatorClient geoLocalisator;

	@Inject
	@RestClient
	private LocalCreditScoreIndexClient creditScoreIndex;

	@Inject
	@RestClient
	private SchufaInterfaceClient schufaInterface;

	@POST
	@Path("conditions")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public CreditCondition getCondition(CreditConditionRequest request) throws InterruptedException, ExecutionException {
		LOG.info("Begin work for Credit Condition Requets {}",request.getContextId());
		
		Future<LocalCreditRating> localRatingFuture = executor.submit(() -> {
			Geolocation loc = geoLocalisator.adressToGeo(request);
			LocalCreditRating localRating = creditScoreIndex.getLocalRating(loc);
			return localRating;
		});

		Future<SchufaRating> schufaFuture = executor.submit(() -> {
			SchufaRating schufa = schufaInterface.getSchufa(request);
			return schufa;
		});

		
		CreditConditionData data = new CreditConditionData(request.getContextId(),localRatingFuture.get(), schufaFuture.get());
		CreditCondition creditConditions = conditionCalculator.getConditions(data);

		
		LOG.info("Finished Work for Credit Condition Request {} : {}",request.getContextId(),creditConditions);
		return creditConditions;
	}

}
