package bc.microservice.services;

import java.util.Random;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import bc.microservice.entities.Geolocation;
import bc.microservice.entities.LocalCreditRating;

@Path("/localCreditIndex")
public class LocalCreditScoreIndex {

private static final Logger LOG = LoggerFactory.getLogger(LocalCreditScoreIndex.class);
	private static final Random RANDOM = new Random(System.currentTimeMillis());
	
	@POST
	@Path("/localRating")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)	
	public LocalCreditRating getLocalRating(Geolocation loc) {
		LOG.info("Loading Credit score of region for {}",loc.getContextId());
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
		}
		LOG.info("Finished Loading Credit score of region for {}",loc.getContextId());
		return new LocalCreditRating(loc.getContextId(), RANDOM.nextDouble());
	}
	
}
