package bc.microservice.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import bc.microservice.entities.Geolocation;
import bc.microservice.entities.LocalCreditRating;

@Path("/localCreditIndex")
@RegisterRestClient(baseUri = "http://localhost:8080")
public interface LocalCreditScoreIndexClient {

	
	@POST
	@Path("/localRating")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)	
	public LocalCreditRating getLocalRating(Geolocation loc);
	
}
