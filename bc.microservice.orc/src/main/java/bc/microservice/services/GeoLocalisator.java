package bc.microservice.services;

import java.util.Random;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import bc.microservice.entities.CreditConditionRequest;
import bc.microservice.entities.Geolocation;

@Path("/geoloc")
public class GeoLocalisator {

private static final Logger LOG = LoggerFactory.getLogger(GeoLocalisator.class);
	private static final Random RANDOM = new Random(System.currentTimeMillis());
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/adressToGeo")
	public Geolocation adressToGeo(CreditConditionRequest request) {
		LOG.info("Converting adress to geo for {}",request.getContextId());
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
		}
		LOG.info("Finished Converting adress to geo for {}",request.getContextId());
		return new Geolocation(request.getContextId(),RANDOM.nextDouble(), RANDOM.nextDouble());
	}
}
