package bc.microservice.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import bc.microservice.entities.CreditCondition;
import bc.microservice.entities.CreditConditionData;

@Path("/conditionsCalc")
public class ConditionsCalculator {
	
	
	private static final Logger LOG = LoggerFactory.getLogger(ConditionsCalculator.class);

	@Path("/calc")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public CreditCondition getConditions(CreditConditionData data) {
		LOG.info("Calculating Conditions for {}",data.getContextId());
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
		}
		LOG.info("Finished Calculating Conditions for {}",data.getContextId());
		return new CreditCondition(data.getContextId(),data.getLocalRating().getRaing() * data.getSchufaRating().getRating());
	}
	
}
