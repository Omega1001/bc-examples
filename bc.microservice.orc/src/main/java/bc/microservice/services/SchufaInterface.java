package bc.microservice.services;

import java.util.Random;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import bc.microservice.entities.CreditConditionRequest;
import bc.microservice.entities.SchufaRating;

@Path("schufa")
public class SchufaInterface {

private static final Logger LOG = LoggerFactory.getLogger(SchufaInterface.class);
	private static final Random RANDOM = new Random(System.currentTimeMillis());
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/rating")
	public SchufaRating getSchufa(CreditConditionRequest request) {
		LOG.info("Reading infos from Schufa for {}",request.getContextId());
		try {
			Thread.sleep(7000);
		} catch (InterruptedException e) {
		}
		LOG.info("Finished Reading infos from Schufa for {}",request.getContextId());
		return new SchufaRating(request.getContextId(),RANDOM.nextDouble());
	}
	
}
