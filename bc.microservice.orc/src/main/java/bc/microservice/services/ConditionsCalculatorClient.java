package bc.microservice.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import bc.microservice.entities.CreditCondition;
import bc.microservice.entities.CreditConditionData;

@Path("/conditionsCalc")
@RegisterRestClient(baseUri = "http://localhost:8080")
public interface ConditionsCalculatorClient {

	@Path("/calc")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public CreditCondition getConditions(CreditConditionData data);
	
}
